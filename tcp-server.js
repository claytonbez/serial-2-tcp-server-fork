var net = require('net');
var EventEmitter = require('events');
var connections = []; 
class tcpServer extends EventEmitter {
    constructor(HOST, PORT) {
        super();
        var self = this;
        self.connections = [];
        self.server = net.createServer(function(socket){
            var remoteAddress = socket.remoteAddress + ':' + socket.remotePort;
            self.emit('connect',remoteAddress);
            connections[remoteAddress] = socket;
            var socketBuff = '';
            socket.on('data', function (data) {
                data = data.toString();
                socketBuff = socketBuff + data;
                if(data.substr(-1) == "\r"){
                    socketBuff = socketBuff.substr(0,socketBuff.length-1);
                    self.emit('data', socketBuff);
                    socketBuff = '';
                }
                
            });
            socket.on('close', function () {
                delete connections[remoteAddress];
                self.emit('close', remoteAddress);
            });
            socket.on('error', function (err) {
                delete connections[remoteAddress]
                self.emit('error', err.message, remoteAddress);
            });
        });
        self.server.listen(PORT, HOST, function () {
            self.emit('start');
        });
        self.server.on('error',function(err){
            console.log('ERRR',err)
        });
    }

    broadcast(data){
        for(var index in connections){
            connections[index].write(data + String.fromCharCode(13));
        }
        
    }
}

module.exports = tcpServer;