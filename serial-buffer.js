class serialBuffer{
    constructor(){
        this.buffer = [];
    }
    push(str){
        this.buffer.push(str);
        return;
    }
    getNext(){
        var item = this.buffer[0]; 
        this.buffer.shift();
        return item;
    }
    length(){
        return this.buffer.length;
    }
}

module.exports = serialBuffer;