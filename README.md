## Serial to TCP server
##### Multiplexer Application

Takes a real serial port and shares it across all connections on a TCP server and other serial ports defined in the config file. 

#### Installation

 - Run installer (.exe for windows and .appImage/.snap for linux
#### Configuration
 - Navigate to the install directory and open the config.json file in a text editor. (Please keep in mind that you cannot deviate from the JSON format during editing, or the application will cease to work)
 
 ```
 {
    "HOST":"0.0.0.0",
    "PORT":21000,
    "SERIALPORT":"COM1",
    "BAUDRATE":115200,
    "DELIMITER":"\n",
    "EXTENDED":[
        {"port":"/dev/ttyCOM2","baud":115200},
        {"port":"/dev/ttyCOM3","baud":57600}
    ]
}
```

The HOST and PORT properties are for the TCP server. (keep host at 0.0.0.0 to access service locally and remotely)

SERIALPORT and BAURATE are for the outgoing 'real' port (external hardware)

DELIMITER is universal across TCP and Serial and will be the character used to differentiate commands running from serial to TCP/other serial ports . vice versa. 

The EXTENDED property is where you define VIRTUAL or OTHER REAL SERIAL PORTS that will act in the same manner a TCP connection to the server will. There is no limit on the amount of ports you can extend. 