var SerialPort = require('serialport');
var EventEmitter = require('events');

class port extends EventEmitter {
    constructor(dev, baud) {
        super(); // required
        var self = this;
        
        self.restarting = 0 ; 
        self.on('write', function (payload) {
            self.port.write(payload + String.fromCharCode(13))
            // console.log('$$send  ', payload);

        });
    }
    setPort(dev, baud) {
        var self = this;
        self.config = {dev:dev,baud:baud};
        self.buffer = [];
        self.port = new SerialPort(dev, { baudRate: baud ,  parser: SerialPort.parsers.readline(String.fromCharCode(13))});
        self.port.on('open', function () {
            self.emit('open');
        });
        self.port.on('close', function () {
            self.emit('close');
            self.restarting = 1; 
            setTimeout(function(){
                self.emit('restart');
                self.readyRestart();
            },5000);
        });
        self.port.on('data', function (payload) {
            payload = payload.toString();
            self.emit('data',payload);
        });
        self.port.on('error', function (error) {
            self.emit('error',error);
            self.restarting = 1; 
            setTimeout(function(){
                self.emit('restart');
                self.readyRestart();
            },5000);
        });
    }
    readyRestart(){
        var self = this;
        self.buffer = [];
        delete self.port;
        self.setPort(self.config.dev,self.config.baud);
    }
}

module.exports = port;