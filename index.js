
//--------------------------------------------------------------------------------
// MODULE REQUIREMENTS
//--------------------------------------------------------------------------------
var pkg = require('./package.json');
var fs = require('fs');
var program = fs.readFileSync("config.json");
program = JSON.parse(program.toString());
var colors = require('colors');
var SerialBuffer = require('./serial-buffer');
var tcpServer = require('./tcp-server');
var serialPort = require('./port');
var logger = require('./logger');
var lg = new logger('logs');
//--------------------------------------------------------------------------------
// DECLARATIONS
//--------------------------------------------------------------------------------
const HOST = program.HOST; //only works where installed.
const PORT = program.PORT;
const BAUD = program.BAUDRATE;
const SERIALPORT = program.SERIALPORT;
const DELIMITER = program.DELIMITER;
//--------------------------------------------------------------------------------
// SETUP
//--------------------------------------------------------------------------------
var serialSendBuffer = new SerialBuffer();
var tcp = new tcpServer(HOST,PORT);

tcp.on('start',function(){
    console.log(`#started tcp server on port ${PORT}`.cyan.bold);
});
tcp.on('connect',function(addr){
    console.log(`#tcp connect ${addr}`.cyan.bold);
    lg.log('TCP CONNECT,' + addr);
});
tcp.on('data',function(data){
    serialSendBuffer.push(data);
    lg.log('TCP,' + data.toString());
});
tcp.on('close',function(addr){
    lg.log('TCP DISCONNECT,' + addr);
    console.log(`#tcp connection closed ${addr}`.yellow.bold);
});

//--------------------------------------------------------------------------------
// REAL SERIAL PORT
//--------------------------------------------------------------------------------
var sp = new serialPort();
sp.setPort(SERIALPORT,BAUD,DELIMITER);
sp.on('data',function(data){
    tcp.broadcast(data);
    vPortsBroadcast(data);
    lg.log('SERIAL,' + data.toString());
});
sp.on('open',function(){
    console.log(colors.green.bold('#serial port open',BAUD,SERIALPORT));
    lg.log('SERIAL PORT OPEN');
    startBufferHandler();
});
sp.on('close',function(){
    lg.log('SERIAL PORT CLOSE');
    console.log(colors.green.bold('#serial port closed',BAUD,SERIALPORT));
    stopBufferHandler();
});
sp.on('error',function(err){
    lg.log('SERIAL PORT ERROR');
    console.log(colors.red.bold('#serial port error',err,SERIALPORT));
    stopBufferHandler();
});
sp.on('restart',function(){
    lg.log('SERIAL RESTART ATTEMPT');
    console.log('#restart serial port - check if used by another program??'.red.bold);
});
//--------------------------------------------------------------------------------
// EXTENDED SERIALPORT - TO ACT AS TCP CONNECTIONS
//--------------------------------------------------------------------------------
program.EXTENDED.forEach(port => {
    
});
var vports = [];
for(let i = 0 ; i < program.EXTENDED.length ; i++){
    vports[i] = new serialPort();
    vports[i].setPort(program.EXTENDED[i].port,program.EXTENDED[i].baud,DELIMITER);
    vports[i].on('open',function(){
        console.log(`#vport open ${program.EXTENDED[i].port}`.magenta.bold);
        lg.log(`VPORT${i} CONNECT,` + program.EXTENDED[i].port);
    });
    vports[i].on('data',function(data){
        serialSendBuffer.push(data);
        lg.log('VPORT${i} ,' + data.toString());
    });
    vports[i].on('close',function(){
        lg.log('VPORT${i} DISCONNECT,' + program.EXTENDED[i].port);
        console.log(`#vport connection closed ${program.EXTENDED[i].port}`.magneta.bold);
    });
}
function vPortsBroadcast(data){
    for(let i = 0 ; i < program.EXTENDED.length ; i++){  
        vports[i].emit('write',data);
    }
}
//--------------------------------------------------------------------------------
// FUNCTIONS
//--------------------------------------------------------------------------------
var bufferRepeater;
function startBufferHandler(){
    bufferRepeater = setInterval(() => {
        if(serialSendBuffer.length()){
            sp.emit('write',serialSendBuffer.getNext());
        }
    }, 0);
}
function stopBufferHandler(){
    clearInterval(bufferRepeater);
}