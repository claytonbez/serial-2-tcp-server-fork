var path = require('path');
var fs = require('fs-extra');
var moment = require('moment');
var FFMT = "YY-MM-DD";
var RFMT = "YYYY-MM-DD,HH:mm:ss";
__dirname = process.cwd();
class logger {
    constructor(dir) {
        var self = this; 
        self.dir = dir;
        self.cf = moment().format(FFMT);
        self.lf = moment().format(FFMT);
        self.configure();
    }
    configure(){
        var self = this; 
        self.folderpath = path.join(__dirname,self.dir);
        self.foldersubpath = path.join(__dirname, self.dir, self.cf);
        self.path = path.join(self.foldersubpath,self.cf + ".csv");
        fs.ensureDir(self.folderpath, function (err) {
            if(err){
                console.warn('Could not create main logs directory');
            }
            self.log('LOGGER SCRIPT START');
            self.startTimer();
        });
    }
    record(data){
        return moment().format(RFMT) + "," + data + "\r\n";
    }
    log(data) {
        var self = this;
        self.cf = moment().format(FFMT);
        self.foldersubpath = path.join(__dirname, this.dir, this.cf);
        self.path = path.join(self.foldersubpath, self.cf + ".csv");
        fs.ensureFile(self.path,function(err){
            if(err){
                console.warn('COuld not create folder/file to log to');
            }
            else{
                fs.appendFile(self.path, self.record(data), function (err) {
                    if (err) {
                        console.warn('Could not append to log file');
                    }
                });
            }
        });
    }
    startTimer(){
        var self = this;
        self.cf = moment().format(FFMT);
        self.timer = setInterval(()=>{
            if(self.lf.substring(6) !== self.cf.substring(6)){
                console.log('DAY CHANGED')
                self.lf = self.cf; 
                self.configure();
            }
        },1000);
    }
}

module.exports = logger;